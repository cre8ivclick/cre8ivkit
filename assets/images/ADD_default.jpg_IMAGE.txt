Add a 'default.jpg' image here, which will be used as the 'default' image throughout the site - for example, it will be used as the default page image for page listings in the Panel.

A good image size is 2000x2000 pixels.
