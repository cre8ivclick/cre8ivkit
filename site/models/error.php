<?php
class ErrorPage extends Page {

        /*
    FIRST IMAGE FILE IN THE PAGE
    This function picks the first available image on the page. If no image
    has been uploaded, it will try to return a 'default.jpg' image, which
    can be found in the site's 'assets/images' folder.
        */
    public function mainImg() {
        if($img = $this->image()){
              return $img;
        } else {
            return asset('assets/images/default.jpg');
        }
    }
}
