<?php

return [

    // Useful while developing, but must set to FALSE
    // before doploying to live production server:
    'debug' => true,
];
