# ProjectTitle

This is the project repository for the development and maintenance of PROJECTNAME. This project was started in PROJECTYEAR.


## Project Organisation

This project uses the [Kirby CMS][kirby], and follows its directory structure. Stylesheets, script files and static media resources are stored inside the `assets` folder. Site templates, view snippets, site content data, as well as all backend-related code is stored in the `site` folder.


## Languages

- **Plain HTML5**, with no special requirements.
- **LESS**, concatenatenated into a single file, compiled into CSS and minified.
- **Javascript**, concatenated, transpiled with [Babel][babel] and minified.
- **PHP**, suitable for deployment in modern servers with PHP ≥ 7.0.
- **SQL**, for use on a MySQL-compatible RDBMS


## Frameworks & Libraries

The frontend templates were built using [UIKit](http://getuikit.com), version 3, the currently stable version. This framework was chosen over others, as it has all of the elements needed to realise the conceptual design.

The backend was built with [Kirby][kirby], version 3. This php-based, flat-file CMS was chosed due to its high-configurability, high compatibility with almost all shared hosting services, extreme ease of maintenance, and a user interface that is fool-proof and highly enjoyable for the client to use.

## Development Tools

All development was done on MacOS X.

During the Design phase, site sketches and mockups were made using [Figma][figma]. Layout placement images used in mockups are from [Unsplash][unsplash].

During the Frontend Development phase, all coding was done in [Sublime Text][sublime] version 3. Post-processing of all project files and automatic compilation of the `build` folder, was made with [CodeKit][codekit], version 3. CodeKit in turn uses several tools internally, such as [npm][npm] to automatically download and maintain 3rd-party frameworks and libraries, and [Babel][babel] for transpiling Javascript, among many others.

During the Backend Development phase, we continued coding with Sublime Text, and used [MAMP Pro](https://www.mamp.info) for continual, local testing. Database modelling was done in [SQLEditor](https://www.malcolmhardie.com/sqleditor/), version 3, which directly outputs the database forge SQL.

## Site Areas & Sections

The site contains a 'Home Page', an internal 'Gobals Section' page (visible only in the site's admin interface), and an 'Error' page. The site also has a Blog, where each article is a sub-page. The site also has a Landing Page builder, which allows the creation of arbitrary pages, as needed.

## Site Functionality
The site has built-in integration with [Matomo](https://matomo.org) for ethical analytics that is respectful of visitors' privacy. The site also has an auto-generated sitemap, as well as '.wellknown' files - such as robots.txt, humans.txt and security.txt - which should assist with SEO efforts. The site's pages also include structured metadata to help search engine crawlers, OpenGraph data to assist with rich-media sharing in social media sites, and is programmed to maximise TTI (time-to-interaction) speed.

[figma]: https://figma.com "Figma"
[sketch]: https://www.sketchapp.com "Sketch"
[hype]: http://tumult.com/hype/ "Tumult Hype"
[unsplash]: https://unsplash.com "Unsplash"
[sublime]: https://www.sublimetext.com "Sublime Text"
[uikit]: https://getuikit.com "UIKit"
[codekit]: https://codekitapp.com "CodeKit"
[jquery]: http://jquery.com "jQuery"
[npm]: https://www.npmjs.com "npm"
[babel]: https://babeljs.io "Babel"
[less]: http://lesscss.org "Less"
[kirby]: https://getkirby.com "KirbyCMS"
